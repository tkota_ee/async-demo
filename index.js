console.log('Before');
getUser(1, (user) => {
    // console.log('User', user);
    getRepositories(user.gitHubUsername, (repos) => {
        console.log('Repository', repos);
    });
    
});
// console.log(user);

console.log('after');

// Callbacks
// Promises
// Async/await

function getUser(id, callback) {
    setTimeout(() => {
        console.log("Reading a user from a database...");
        callback({ id: id, gitHubUsername: 'kota'});

    }, 2000);
};

function getRepositories(username, callback) {
    setTimeout(() => {
        console.log("calling Github API...");
        callback(['repo1', 'repo2', 'repo3']);
    }, 2000);
    
}
















